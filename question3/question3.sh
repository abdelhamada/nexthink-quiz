#!/bin/sh

if [ -z $1 ]; then
  echo "Please enter a valid user name"
  exit 0
fi

w=`who | grep $1`

if [ -z "$w" ] 
then
    echo "$1 is not logged in"
else
    echo "$1 is currently logged in"
fi
