# Question 2  

In a bash console run the following:  
```bash
cat messages.log | grep -i -e 'warning' -e 'critical' -e 'error' | tee result.log  
```
_Note: if we want to print the result in the console, we should remove the "| tee result.log" from the command_ 
  

In a powershell console run  
```bash
type messages.log | findstr /I "ERROR CRITICAL WARNING" > result.log 
```
_Note: if we want to print the result in the console, we should remove the "> result.log" from the command_ 