const fs = require('fs');

const startingLine = 'starting nxengine';
const endLine = 'nxengine is running';

const ret = [];
const pattern = /[a-zA-Z]{3}  \d \d\d:\d\d:\d\d/;

// Read the engine.log file line by line
const logs = fs.readFileSync('engine.log').toString().split('\n');

// Filter the logs to keep only the lines needed
const filterredLogs = logs.filter(line => {
    return line.includes(startingLine) || line.includes(endLine);
});
let j = 1;
for (let i = 0; i < filterredLogs.length; i += 2) {
    const startTime = filterredLogs[i].match(pattern)[0];
    const endTime = filterredLogs[i + 1].match(pattern)[0];
    const start = new Date(startTime);
    const end = new Date(endTime);
    let diff;
    // If the process starts and ends in the same day calculate the duration
    if (end.getDay() === start.getDay()) {
        diff = end.getTime() - start.getTime();
    }
    // If the process ends the day after
    else {
      diff = (end.getTime() + (24 * 60 * 60 * 1000)) - start.getTime();
    }
    const time = new Date(diff).toISOString().substr(11, 8)
    ret.push(`StartCycle: ${j} Duration: ${time}`);
    j++;
}

// Write the result in a file
fs.writeFileSync('question1-result.log', ret.join('\n'));
